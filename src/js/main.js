(function () {
  // Env
  const electron = require('electron')
  const exec = require('child_process').exec
  const { ipcRenderer } = electron
  // Element
  const tabManage = document.querySelectorAll('.tab-manage')
  const contentManage = document.querySelectorAll('.content-manage')
    // const taskTodo = document.querySelector('.task-todo')
  const closeDom = document.querySelector('.close')
  // Element-Button
  const birdBro = document.querySelector('.add-birdbro')
  const fakeMDM = document.querySelector('.add-fakemdm')
  const doubleOnline = document.querySelector('.add-doubleonline')
  const reboot = document.querySelector('.reboot-recovery')

  const date = new Date(), nowTime = date.getTime()

  // Date
  document.querySelector('.date').innerText = `${date.getFullYear()}/${date.getMonth()+1}/${date.getDate()}`

  // BindEvent
  tabManage.forEach((el,index) => {
    el.addEventListener('click', () => {
      activeTab (index)
      activeContent (index)
    })
  })

  // CompleteOrDelTodo
  // taskTodo.addEventListener('click', (event) => {
  //     activeTab (0)
  //     activeContent (0)
  // })

  // MinimizeWindow
  closeDom.addEventListener('click', () => {
    ipcRenderer.send('mainWindow:close')
  })

  // DownloadBirdBro
  birdBro.addEventListener('click', () => {
    addBirdBro()
  })

  // DowloadFakeMDM
  fakeMDM.addEventListener('click', () => {
    addFakeMDM()
  })

  // DownloadDoubleOnline
  doubleOnline.addEventListener('click', () => {
    addDoubleOnline()
  })

  // RebootRecovery
  reboot.addEventListener('click', () => {
    rebootRecovery()
  })


  function activeTab(index) {
    tabManage.forEach((tabEl) => {
      tabEl.classList.remove('nav-active')
    })
    tabManage[index].classList.add('nav-active')
  }

  function activeContent(index) {
    contentManage.forEach((taskEl) => {
      taskEl.classList.remove('content-active')
    })
    contentManage[index].classList.add('content-active')
    taskName.value = ''
    taskTime.value = ''
  }

  function addBirdBro() {
    let cmd = 'adb install E:/Program/APK/util_apk/bird_bro.apk'
    exec(cmd, () => {})
    alert('Downloading...')
  }

  function addFakeMDM() {
    let cmd = '"adb install -r -d -t E:/Program/APK/util_apk/fake_mdm.apk"'
    exec(cmd, () => {})
    alert('Downloading...')
  }

  function addDoubleOnline() {
    let cmd = 'adb install -r -d -t E:/Program/APK/util_apk/test_1.0.apk'
    exec(cmd, () => {})
    alert('Downloading...')
  }

  function rebootRecovery() {
    let cmd = 'adb reboot recovery'
    exec(cmd, () => {})
    alert('Reboot')
  }
})();



